from tinydb import TinyDB,Query
import os 

def inserir_db (titulo,data,horario,link,tag,paragrafos):
    db=TinyDB("/home/mki_anaferreira/codigo/fundamentos-de-ciencia-de-dados/fundamentos-python/senado.json", indent=4, ensure_ascii=False)
    buscar=Query ()
    verificar_db=db.contains(buscar.titulo==titulo)
    if not verificar_db:
        db.insert({
        "titulo":titulo,
        "data": data,
        "horario": horario,
        "link":link,
        "tag":tag,
        "paragrafo":paragrafos
        })
    else: 
        print("Já está na base")

def atualizar_db (bd_json="NA",titulo="NA",paragrafo="NA"):
    dir_json=f"/home/mki_anaferreira/codigo/fundamentos-de-ciencia-de-dados/fundamentos-python/{bd_json}"
    db=TinyDB(dir_json, indent=4, ensure_ascii=False)
    buscar= Query()
    for info in db:
        db.upsert({
            "paragrafo":paragrafo
        },buscar.titulo==titulo)

def main ():
    #inserir_db ()
    atualizar_db ()

if __name__ == "__main__":
    main ()
