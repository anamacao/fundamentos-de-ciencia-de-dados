from tinydb import TinyDB,Query
import os 
from banco_json import atualizar_db

def tratar_banco_senado (bd_json):
    dir_json=f"/home/mki_anaferreira/codigo/fundamentos-de-ciencia-de-dados/fundamentos-python/{bd_json}"
    db=TinyDB(dir_json, indent=4, ensure_ascii=False)
    buscar=Query()
    paragrafo_vazio=[]
    for info in iter(db):
        titulo= info ["titulo"]
        paragrafo=info["paragrafo"]
        if "" in paragrafo:
            paragrafo_vazio.append (paragrafo)
            paragrafo=list(filter(None,paragrafo))
            db.upsert({
            "paragrafo":paragrafo
        },buscar.titulo==titulo)
            #atualizar_db (bd_json,titulo,paragrafo)
    print (len(paragrafo_vazio))

def tratar_banco (bd_json):
    dir_json=f"/home/mki_anaferreira/codigo/fundamentos-de-ciencia-de-dados/fundamentos-python/{bd_json}"
    db=TinyDB(dir_json, indent=4, ensure_ascii=False)
    buscar=Query()
    for info in iter(db):
        titulo= info["titulo"]
        data=info["data"].replace("\n","").strip()
        #horario=info["horario"].replace("\n","").strip()
        db.upsert({
            "data":data
        },buscar.titulo==titulo)







def main ():
    bd_json="db_noticia.json"
    tratar_banco (bd_json)

if __name__ == "__main__":
    main ()