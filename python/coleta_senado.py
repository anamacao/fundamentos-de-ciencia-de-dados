import requests 
from bs4 import BeautifulSoup
import os 
from banco_json import inserir_db

def acessar_pagina (url):
    pagina = requests.get(url)
    bs=BeautifulSoup (pagina.text, "html.parser")
    #print (bs)
    return bs

def extrair_info ():
    lista_paginas= paginas()
    for index,pagina in enumerate (lista_paginas,start=1):
        print(index,pagina)
        bs = acessar_pagina (pagina)
        noticias=bs.find("div",attrs={"id":"textoMateria"}).find("ol",attrs={"class":"list-unstyled lista-resultados"}).find_all("li")
        # print(noticias)
        for noticia in noticias: 
            try:    
                titulo= noticia.find("span", attrs={"class":"eta normalis-xs"}).text
                print(titulo)
            except:
                titulo= noticia.a.span.text
                print(titulo)
            data_horario = noticia.find ("span", attrs={"class":"text-muted normalis-xs"}).text
            data = data_horario[:10]
            horario = data_horario[11:]
            print (horario)
            print (data)
            dominio = "https://www12.senado.leg.br"
            link = dominio+noticia.a["href"]
            print (link)
            tag = noticia.find ("span", attrs={"class":"normalis-xs"}).text
            print (tag)

            page_noticia =acessar_pagina (link)
            paragrafos = [] #extração dos paragrafos 
            
            try:
                paragrafos = [p.text.strip() for p in page_noticia.find("div", {"class":"js-Materia"}).find_all("p")]
                print (F'PARÁGRAFOS 1: {paragrafos[1:]}')
            except:
                pass 

            if not paragrafos:
                try:
                    paragrafos = [p.text.strip() for p in page_noticia.find("div", {"class":"content-container"}).find_all("p")]
                    print (f'PARÁGRAFOS 2: {paragrafos[:]}')
                except: 
                    pass

            if not paragrafos:
                try:
                    paragrafos = [p.text.strip() for p in page_noticia.find("div", {"id" : "textoMateria"}).find_all("p")]
                    paragrafos = [paragrafo for paragrafo in paragrafos if (paragrafo != "Mais informações a seguir") if (paragrafo != "Agência Senado (Reprodução autorizada mediante citação da Agência Senado)")]
                    print(f'PARAGRÁFOS 3: {paragrafos}')
                    if not paragrafos:
                        paragrafos = ["NA"]
                    ## EXTRAÇÃO
                except:
                    paragrafos = ["NA"]

            print("------------ PRÓXIMA NOTÍCIA ---------------")
            
            paragrafo = []
            #for paragrafo in paragrafos:
              #paragrafo.append (paragrafo.text)
            #print (paragrafo)
            print ("###")
            #data_normalizada=ajustar_data(data)
            inserir_db (titulo,data,horario,link,tag,paragrafos)

def paginas ():
    paginas=[]
    contador=1267
    while contador >=1:
        url="https://www12.senado.leg.br/noticias/ultimas/"
        url=url+str(contador)
        paginas.append(url)
        contador=contador-1
    #print (paginas)
    return paginas
    



def main ():
    extrair_info ()
    #paginas()



if __name__ == "__main__":
    main ()
