import requests
from bs4 import BeautifulSoup

def acessar_pagina(url):
    pagina=requests.get(url)
    bs=BeautifulSoup (pagina.text, "html.parser")
    return bs

def paginas():
    paginas = []
    contador = 1267
    while contador >=0:
        if contador != 0:
            url = "https://www12.senado.leg.br/noticias/ultimas"
            url = url + str(contador)
            contador = contador - 1
            paginas.append(url)
        elif contador == 0:
            url = "https://www12.senado.leg.br/noticias/ultimas" 
            contador = contador -1
            paginas.append(url)
    print (paginas)
    return paginas


def coleta_senado_noticias():
    for index, pagina in enumerate(paginas(), start=1):
        print(index, pagina)
        bs=acessar_pagina(pagina)
        lista_de_tags_artigos=bs.find_all ("article") 
        for tag_articule in lista_de_tags_artigos:
            titulo=tag_articule.a["title"]
            link=tag_articule.a["href"]
            print (titulo)
            print (link)
            print ("#########")


def main ():
    #paginas()
    coleta_senado_noticias()

if __name__ == "__main__":
    main ()