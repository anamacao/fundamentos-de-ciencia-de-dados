# Fundamentos de ciência de dados

## versionamento

# digitar os comandos abaixo na raiz do projeto para versionar o codigo e subir no gitlab 

```
git add .
git commit -m "insira mensagem aqui"
git pull origin main
git push origin main

```

## ambientevirtual

```
conda activate env_ana
conda env update


```

## CONFERIR EXTRAÇÃO DE PARÁGRAFOS (4)
------------ PRÓXIMA NOTÍCIA ---------------
A Comissão de Meio Ambiente aprovou requerimentos para avaliar e discutir ações do governo federal no combate ao desmatamento, em função do aumento de queimadas nos biomas Cerrado, Amazônia e Pantanal (REQs 04 e 06/2021).
02/06/2021 15h20
https://www12.senado.leg.br/noticias/senado-agora/2021/06/02/controle-do-desmatamento
Senado Agora
PARAGRÁFOS 3: ['Mais informações a seguir', 'Agência Senado (Reprodução autorizada mediante citação da Agência Senado)']
------------ PRÓXIMA NOTÍCIA ---------------
A Comissão de Meio Ambiente (CMA) aprovou requerimentos para analisar a Política Nacional de Mudanças do Clima e os compromissos assumidos pelo Brasil no âmbito do Acordo de Paris (REQs 2 e 3/2021).
02/06/2021 15h18
https://www12.senado.leg.br/noticias/senado-agora/2021/06/02/acordo-de-paris
Senado Agora
PARAGRÁFOS 3: ['Mais informações a seguir', 'Agência Senado (Reprodução autorizada mediante citação da Agência Senado)']